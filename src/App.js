import './App.css';
import FetchItems from './components/fetchItems';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <FetchItems />
    </div>
  );
}

export default App;
