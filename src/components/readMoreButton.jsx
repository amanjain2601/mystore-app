import React from 'react';
import { useRef } from 'react';

const ReadButton = ({ text, id }) => {
  let modalBoxRef = useRef(null);
  function boxDisplay(id) {
    modalBoxRef.current.style.display =
      modalBoxRef.current.style.display === 'none' ? 'block' : 'none';
  }

  return (
    <>
      <div className="read-btn-container">
        <button className="btn btn-dark m-3 " onClick={() => boxDisplay(id)}>
          Read More Here
        </button>
      </div>
      <div ref={modalBoxRef} className="modal-box">
        <p>{text}</p>
      </div>
    </>
  );
};

export default ReadButton;
