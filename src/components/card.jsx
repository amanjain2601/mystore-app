import React from 'react';
import ReadButton from './readMoreButton';

const Card = ({ currentItem }) => {
  return (
    <div className="card">
      <img src={currentItem.image} className="card-img-top" alt="img" />
      <div className="card-body">
        <h5 className="card-title m-2">{currentItem.title}</h5>
        <h5 className="card-title m-2">${currentItem.price}</h5>
        <h6 className="card-subtitle mb-2 m-2 text-muted">
          {currentItem.category}
        </h6>
        <p className="card-text">{currentItem.description}</p>
        <ReadButton text={currentItem.description} id={currentItem.id} />
      </div>
    </div>
  );
};

export default Card;
