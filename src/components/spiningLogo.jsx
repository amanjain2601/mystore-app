import React from 'react';

const SpinningLogo = () => {
  return (
    <div className="text-center m-5 fs-1">
      <div className="spinner-border" role="status">
        <span className="visually-hidden">Loading...</span>
      </div>
    </div>
  );
};

export default SpinningLogo;
