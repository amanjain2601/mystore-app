import React from 'react';
import myStore from '../myStore.jpg';

const Navigation = ({ onInput }) => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <a className="navbar-brand" href="./">
          <img className="logo" src={myStore} alt="" />
        </a>

        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link active" aria-current="page" href="./">
                Home
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link active" aria-current="page" href="./">
                About
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link active" aria-current="page" href="./">
                Contact Us
              </a>
            </li>
          </ul>
        </div>

        <form className="d-flex" id="nav-search">
          <input
            onInput={onInput}
            className="form-control me-2"
            type="search"
            placeholder="Search By Category.."
            aria-label="Search"
          />
        </form>
      </div>
    </nav>
  );
};

export default Navigation;
