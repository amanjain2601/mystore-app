import React, { Component } from 'react';
import Display from './display';

const axios = require('axios');
let dataCopy;

class FetchItems extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: '',
    };
  }

  getData = () => {
    axios
      .get('https://fakestoreapi.com/products')
      .then((response) => {
        let dataReturned = response.data;
        return dataReturned;
      })
      .then((dataReturned) => {
        this.setState(
          {
            data: dataReturned,
          },
          () => {
            dataCopy = [...dataReturned];
          }
        );
      })
      .catch((error) => {
        // handle error
        this.setState({
          data: 'Error',
        });
      });
  };

  postData = (jsonReturned) => {
    dataCopy = [...dataCopy, jsonReturned];
    this.setState({
      data: dataCopy,
    });
  };

  updateData = (jsonReturned) => {
    let foundIndex;
    dataCopy.find((current, index) => {
      if (current.id === Number(jsonReturned.id)) {
        foundIndex = index;
        return true;
      } else {
        return false;
      }
    });

    dataCopy = [...dataCopy];
    dataCopy[foundIndex] = jsonReturned;

    this.setState({
      data: dataCopy,
    });
  };

  searchItems = (event) => {
    let newListOfItems = dataCopy.filter((currentData) => {
      return currentData.category.indexOf(event.target.value) !== -1;
    });

    if (newListOfItems.length !== 0) {
      this.setState({
        data: newListOfItems,
      });
    } else {
      this.setState({
        data: 'No Items Related to search',
      });
    }
  };

  componentDidMount() {
    this.getData();
  }

  render() {
    return (
      <div>
        <Display
          dataRecieved={this.state.data}
          onInput={this.searchItems}
          postData={this.postData}
          updateData={this.updateData}
        />
      </div>
    );
  }
}

export default FetchItems;
