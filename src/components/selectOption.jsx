import React from 'react';

const SelectOption = ({ id }) => {
  return <option>{id}</option>;
};

export default SelectOption;
