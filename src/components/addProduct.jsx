import React, { Component } from 'react';
import SelectOption from './selectOption';
const axios = require('axios');

class AddProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      price: '',
      description: '',
      image: '',
      category: '',
    };

    this.inputIDRef = React.createRef();
  }

  onChangeHAndler = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  addProductHandler = (event) => {
    event.preventDefault();

    axios
      .post('https://fakestoreapi.com/products', this.state)
      .then((response) => {
        return response.data;
      })
      .then((json) => {
        this.props.postData(json);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  onInputID = (event) => {
    let id = event.target.value;

    axios
      .put(`https://fakestoreapi.com/products/${id}`, this.state)
      .then((response) => {
        return response.data;
      })
      .then((json) => {
        this.props.updateData(json);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  updateProductHandler = (event) => {
    event.preventDefault();

    this.inputIDRef.current.style.display = 'block';
  };

  render() {
    return (
      <div>
        <h3>Add Product Here</h3>
        <form action="#">
          <div className="add-product-container">
            <select
              className="select-id"
              ref={this.inputIDRef}
              style={{ display: 'none' }}
              onChange={this.onInputID}
            >
              <option>Select Id to Update</option>
              {this.props.dataRecieved.map((current) => (
                <SelectOption key={current.id} id={current.id} />
              ))}
            </select>
            <input
              type="text"
              name="title"
              placeholder="Enter Title"
              onChange={this.onChangeHAndler}
            />
            <input
              type="number"
              name="price"
              placeholder="Enter Product Price"
              onChange={this.onChangeHAndler}
            />
            <input
              type="text"
              name="description"
              placeholder="Enter Description"
              onChange={this.onChangeHAndler}
            />
            <input
              type="text"
              name="image"
              placeholder="Enter Image URL"
              onChange={this.onChangeHAndler}
            />
            <input
              type="text"
              name="category"
              placeholder="Enter category"
              onChange={this.onChangeHAndler}
            />
          </div>
          <button
            onClick={this.addProductHandler}
            className="btn btn-warning m-3 add-button"
          >
            Add Product
          </button>
          <button
            onClick={this.updateProductHandler}
            className="btn btn-warning m-3 update-button"
          >
            Update Product
          </button>
        </form>
      </div>
    );
  }
}

export default AddProduct;
