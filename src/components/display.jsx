import React from 'react';
import Navigation from './navigation';
import SpinningLogo from './spiningLogo';
import Card from './card';
import Footer from './footer';
import onlineShopLogo from '../logo3.jpg';
import FastDelivery from '../del.png';
import FreeDelivery from '../delv3.jpg';
import Online from '../online.png';
import BestPrice from '../bestprice.jpg';
import AddProduct from './addProduct';

const Display = ({ dataRecieved, onInput, postData, updateData }) => {
  function DisplayUI() {
    if (dataRecieved === 'Error') {
      return <h1 className="text-danger">Error</h1>;
    } else if (dataRecieved === '') {
      return <SpinningLogo />;
    } else if (dataRecieved.length === 0) {
      return <h1>No Items Found..!!</h1>;
    } else if (dataRecieved === 'No Items Related to search') {
      return (
        <>
          <Navigation onInput={onInput} />
          <h1>Sorry..No Items Found related to your search</h1>
          <Footer />
        </>
      );
    } else {
      return (
        <>
          <Navigation onInput={onInput} />
          <div id="logos-container" className="logos">
            <div className="shopping-logo">
              <img src={onlineShopLogo} alt="logo" />
            </div>
            <div className="heading">
              <h1 className="m-2 ">Our Services</h1>
            </div>
            <div className="service-logo">
              <img src={FastDelivery} alt="logo" />
              <img src={FreeDelivery} alt="logo" />
              <img src={Online} alt="logo" />
              <img src={BestPrice} alt="logo" />
            </div>
          </div>

          <div className="heading">
            <h1 className="m-2 our-store-heading">Our Store</h1>
          </div>
          <div className="cards-container">
            {dataRecieved.map((currentItem) => {
              return <Card key={currentItem.id} currentItem={currentItem} />;
            })}
          </div>
          <AddProduct
            postData={postData}
            dataRecieved={dataRecieved}
            updateData={updateData}
          />
          <Footer />
        </>
      );
    }
  }

  return <div>{DisplayUI()}</div>;
};

export default Display;
